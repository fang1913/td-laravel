@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('commun.modifier') }}
                </div>
                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    @include('etudiant._form')
                    {{ csrf_field() }}
                    <th>
                        {!!'
                        <p class="error-msg">
                            '. $errors->first(trans('etudiant.nom'), ':message ').$errors->first(trans('etudiant.prenom'), ':message') .'
                        </p>
                        ' !!}
                    </th>
                    <th>
                        <a class="btn btn-sm btn-primary m-t-n-xs" href="{{ route('home', $etudiant->id) }}">
                            {{ trans('commun.dashboard') }}
                        </a>
                    </th>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
