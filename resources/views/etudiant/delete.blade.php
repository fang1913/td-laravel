@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{'Détails'}}
                </div>
                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                @if(isset($etudiant))

                {!! Form::model($etudiant, 
                    ['route' => ['destroyEtudiant', $etudiant->id], 'method'=> 'delete']) !!}
                    
                @else
                {!! Form::open(['route' => 'home']) !!}
                @endif
                    <th>
                        {!!'<p><strong>'. trans('etudiant.prenom').' : ' . $etudiant->prenom .'</strong></p>'!!}
                    </th>

                    <th>
                       {!!'<p><strong>'.trans('etudiant.nom').' : ' . $etudiant->nom .'</strong></p>'!!}
                    </th>

                      <th>
                        <a class="btn btn-sm btn-primary m-t-n-xs" href="{{ route('home', $etudiant->id) }}"> {{ trans('commun.dashboard') }} </a>
                    </th>

                    <button class="btn btn-sm btn-primary m-t-n-xs" type="submit">
                        {{ trans('commun.supprimer') }}
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
