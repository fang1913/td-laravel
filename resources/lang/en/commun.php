<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

   'enregistrer' => 'Save',
    'details' => 'Details',
    'modifier' => 'Modify',
    'supprimer' => 'Delete',
    'dashboard' => 'Dashboard',
    'action' => 'Action',
    'home' => 'Home',
    'login' => 'Login',
    'logout' => 'Logout',
    'register' => 'Register',
    'email' => 'E-Mail Address',
    'password' => 'Password',
    'confirmpassword' => 'Confirm Password',
    'name' => 'Name',
    'rememberme' => 'Remember me',
    'forgotpassword' => 'I forgot my password !',
    'resetpassword' => 'Reset Password',
    'sendlink' => 'Send Password Reset Link',
    'lang' => 'EN'



];
