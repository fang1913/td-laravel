<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

   'enregistrer' => 'Enregistrer',
    'details' => 'Détails',
    'modifier' => 'Modifier',
    'supprimer' => 'Supprimer',
    'dashboard' => 'Tableau de bord',
    'action' => 'Action',
    'home' => 'Acceuil',
    'login' => 'S\'identifier',
    'logout' => 'Se déconnecter',
    'register' => 'S\'enregistrer',
    'email' => 'Adresse E-Mail',
    'password' => 'Mot de passe',
    'confirmpassword' => 'Retaper le mot de passe',
    'name' => 'Nom',
    'rememberme' => 'Se rappeler de moi',
    'forgotpassword' => 'j\'ai oublié mon mot de passe !',
    'resetpassword' => 'Réinitialiser mon mot de passe',
    'sendlink' => 'M\'envoyer le lien de Réinitialiser de mon mot de passe',
    'lang' => 'FR',

];
