<?php


use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::create([
        	'name' => 'Arfang FAYE',
        	'email' => 'fangfang1913@gmail.com',
        	'password' => bcrypt('passer'),
        ]);
    }
}
