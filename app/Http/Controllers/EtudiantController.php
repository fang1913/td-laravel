<?php
  
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;

    use Illuminate\Http\Request;
      use App\Etudiant;

    class EtudiantController extends Controller
    {
    
        public function __construct()
        {
            $this->middleware('auth');
        }

        //ajout d'un etudiant dans le systeme
        public function add(Request $request){

            $nom = request('nom');
            $prenom = request('prenom');

            //validation des entrés, le nom et le prenom sont required
            Validator::make(compact('nom', 'prenom'), ['nom' => ['required'], 'prenom' => ['required'] ]
            )->validate();

            Etudiant::create($request->all());
            return back()->with('status', trans('etudiant.msgenregistrementok'));
        }

        //affichage des information des etudiants
            public function show($id){

            $etudiant = Etudiant::findorfail($id);
            return view('etudiant.show', compact('etudiant'));
        }

        //redirection vers la vue edit 
        public function edit($id) {

            $etudiant = Etudiant::findorfail($id);
            return view('etudiant.edit', compact('etudiant'));
        }

        //mise a jour des informations d'un etudiant
        public function update(Request $request, $id){

            $etudiant = Etudiant::findorfail($id);

            $nom = request('nom');
            $prenom = request('prenom');

            //validation des entrees
            Validator::make(
                compact('nom', 'prenom'), ['nom' => ['required'], 'prenom' => ['required'] ]
            )->validate();

            $etudiant->nom = $request->input('nom');
            $etudiant->prenom = $request->input('prenom');

            //enregistrement des nouveaux informations dans la base de donnee
            if($etudiant->save()){

                $etudiants = Etudiant::all();
                return redirect()->route('home', compact('etudiants'));

            } else {
                return back()->with('status', trans('etudiant.msgmiseajourok'));
            }            
        }

        //redirection vers la vue suppression
        public function delete($id) {  //Request $request, 
            // Etudiant::destroy($id);
            // $etudiant = Etudiant::findorfail($i
            // return back()->with('status', trans('etudiant.msgmiseajourok'));

            $etudiant = Etudiant::findorfail($id);
            return view('etudiant.delete', compact('etudiant'));;
        }


        //suppression d'un dans la base en fonction de l'identifiant
           public function destroy(Request $request, $id) {  //
            $success = Etudiant::destroy($id);
            // $etudiant = Etudiant::findorfail($i
            if($success != 0){
                $etudiants = Etudiant::all();
                // return redirect('http://anotherdomain.com/');
                return redirect()->route('home', compact('etudiants'));
            }else{
                return back()->with('status', trans('etudiant.msgmiseajourok'));
            }
            
        }

    }
