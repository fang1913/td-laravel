<?php

use App\Etudiant;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('etudiants/add', 'EtudiantController@add')->name('addEtudiant');

Route::get('etudiants/show/{id}', 'EtudiantController@show')->name('showEtudiant')->where('id', '[0-9]+');

Route::get('etudiants/edit/{id}', 'EtudiantController@edit')->name('editEtudiant')->where('id', '[0-9]+');

Route::put('etudiants/update/{id}', 'EtudiantController@update')->name('updateEtudiant')->where('id', '[0-9]+');

Route::get('etudiants/delete/{id}', 'EtudiantController@delete')->name('deleteEtudiant')->where('id', '[0-9]+');

Route::delete('etudiants/destroy/{id}', 'EtudiantController@destroy')->name('destroyEtudiant')->where('id', '[0-9]+');

Route::get('lang/{lang}', function($lang) {
  \Session::put('lang', $lang);
  return \Redirect::back();
})->middleware('web')->name('change_lang');

// Route::delete('etudiants/delete/{id}', function ($id) {
// 	Etudiant::destroy($id);
//     // return back()->with('status', trans('etudiant.msgmiseajourok'));
//     return view('welcome');
// })->name('deleteEtudiant')->where('id', '[0-9]+');

			
